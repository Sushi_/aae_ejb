package io.github.ephys.hasher;

public class HashException extends RuntimeException {

  private static final long serialVersionUID = -143552357299845134L;

  public HashException() {
  }

  public HashException(String message) {
    super(message);
  }

  public HashException(Throwable cause) {
    super(cause);
  }

  public HashException(String message, Throwable cause) {
    super(message, cause);
  }

  public HashException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
