package be.ipl.wazabi.core;

public class WazabiException extends RuntimeException {

  private static final long serialVersionUID = 9033686682544266918L;

  public WazabiException() {
    super();
  }

  public WazabiException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public WazabiException(String message, Throwable cause) {
    super(message, cause);
  }

  public WazabiException(String message) {
    super(message);
  }

  public WazabiException(Throwable cause) {
    super(cause);
  }
}
