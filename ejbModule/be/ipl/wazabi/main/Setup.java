package be.ipl.wazabi.main;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.transform.stream.StreamSource;

import be.ipl.wazabi.core.FatalException;
import be.ipl.wazabi.dal.dao.ICardDao;
import be.ipl.wazabi.dal.dao.ICardEffectDao;
import be.ipl.wazabi.dal.dao.IDiceDao;
import be.ipl.wazabi.entity.CardEffectEntity;
import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.main.xml.Carte;
import be.ipl.wazabi.main.xml.Face;
import be.ipl.wazabi.main.xml.Wazabi;
import be.ipl.wazabi.ucc.IUserUcc;
import io.github.ephys.hasher.IStringHasher;
import io.github.ephys.hasher.pbkdf2.Pbkdf2Hasher;

@Startup
@Singleton
public class Setup {

  @EJB
  private IDiceDao diceDao;

  @EJB
  private ICardEffectDao cardEffectDao;

  @EJB
  private ICardDao cardDao;

  @EJB
  private IUserUcc userUcc;

  @EJB
  private IStringHasher hashHandler;

  private Wazabi wazabi;
  private int faceCountWazabi;
  private int faceCountCard;
  private int faceCountDice;

  @PostConstruct
  private void init() {
    
    try {
      InputStream is = new FileInputStream(
          "../standalone/deployments/AAE_EAR.ear/AAE_EJB.jar/META-INF/wazabi.xml");
      wazabi = fromStream(Wazabi.class, is);

      System.out.println("[Init] Inserting dices.");
      for (int i = 0; i < wazabi.getDe().getNbTotalDes(); i++) {
        DiceEntity dice = new DiceEntity();
        diceDao.insert(dice);
      }

      System.out.println("[Init] Inserting cards.");
      for (Carte carte : wazabi.getCarte()) {
        CardEffectEntity cardEffect = new CardEffectEntity(carte.getCodeEffet(), carte.getCout(),
            carte.getEffet(), getDescription(carte.getContent()));
        cardEffectDao.insert(cardEffect);

        int nbCartesDeCeType = carte.getNb();
        for (int i = 0; i < nbCartesDeCeType; i++) {
          CardEntity card = new CardEntity(cardEffect);
          cardDao.insert(card);
        }
      }
      
      System.out.println("[Init] Hash algorithm is " + Pbkdf2Hasher.class.getCanonicalName());
      hashHandler.addHashAlgorithm("pbkdf2", new Pbkdf2Hasher(1000));
      hashHandler.setPreferedAlgorithm("pbkdf2");
      
      System.out.println("[Init] Creating accounts");
      userUcc.register("em", "em");
      userUcc.register("mi", "mi");
      userUcc.register("ol", "ol");
      
      List<Face> dices = wazabi.getDe().getFace();
      for (Face dice : dices) {
        switch (dice.getIdentif()) {
          case "w":
            faceCountWazabi = dice.getNbFaces();
            break;
          case "d":
            faceCountDice = dice.getNbFaces();
            break;
          case "c":
            faceCountCard = dice.getNbFaces();
            break;
        }
      }
    } catch (Exception e) {
      throw new FatalException(e);
    }
  }

  private String getDescription(List<Serializable> liste) {
    StringBuilder stringBuilder = new StringBuilder();

    Iterator<Serializable> it = liste.iterator();
    while (it.hasNext()) {
      Serializable xmlElement = it.next();

      if (xmlElement instanceof String) {
        stringBuilder.append(xmlElement);
      } else if (xmlElement instanceof Carte.Figure) {
        String ref = ((Carte.Figure) xmlElement).getRef();

        for (Face face : wazabi.getDe().getFace()) {
          if (face.getIdentif().equals(ref)) {
            stringBuilder.append(face.getFigure());
            break;
          }
        }
      }
    }

    return stringBuilder.toString();
  }


  @SuppressWarnings("unchecked")
  private static <T> T fromStream(Class<T> clazz, InputStream input) throws JAXBException {
    JAXBContext ctx = JAXBContext.newInstance(clazz);
    Object result = ctx.createUnmarshaller().unmarshal(new StreamSource(input), clazz);
    if (result instanceof JAXBElement<?>) {
      result = JAXBIntrospector.getValue(result);
    }

    return (T) result;
  }

  public int getMinPlayers() {
    return wazabi.getMinJoueurs();
  }

  public int getMaxPlayers() {
    return wazabi.getMaxJoueurs();
  }

  public int getStartCardCount() {
    return wazabi.getNbCartesParJoueur();
  }

  public int getStartDiceCount() {
    return wazabi.getDe().getNbParJoueur();
  }

  public int getFaceCountWazabi() {
    return faceCountWazabi;
  }

  public int getFaceCountCard() {
    return faceCountCard;
  }

  public int getFaceCountDice() {
    return faceCountDice;
  }
}
