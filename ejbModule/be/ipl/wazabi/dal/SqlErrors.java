package be.ipl.wazabi.dal;

public final class SqlErrors {
  
  public static final String UNIQUE_VIOLATION = "23505";
  
  private SqlErrors() {}
}
