package be.ipl.wazabi.dal.exception;

public class ConstraintViolationException extends Exception {

  private static final long serialVersionUID = -2541192593965651037L;

  public ConstraintViolationException(String message, Throwable cause) {
    super(message, cause);
  }
  
  @Override
  public synchronized Throwable fillInStackTrace() {
    // Do nothing, we don't need a stacktrace for these exception as they always have a cause.
    return this;
  }
}
