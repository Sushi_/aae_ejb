package be.ipl.wazabi.dal.exception;

public class NonUniqueException extends ConstraintViolationException {

  private static final long serialVersionUID = -2541192593965651037L;

  public NonUniqueException(String message, Throwable cause) {
    super(message, cause);
  }
}
