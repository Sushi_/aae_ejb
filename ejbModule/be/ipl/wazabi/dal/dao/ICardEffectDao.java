package be.ipl.wazabi.dal.dao;

import javax.ejb.Local;

import be.ipl.wazabi.entity.CardEffectEntity;

@Local
public interface ICardEffectDao extends IBaseDao<Integer, CardEffectEntity> {

}
