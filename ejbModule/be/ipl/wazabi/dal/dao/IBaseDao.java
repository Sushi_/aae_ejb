
package be.ipl.wazabi.dal.dao;

import java.io.Serializable;
import java.util.List;

public interface IBaseDao<K, E> extends Serializable {

  E getById(K id);

  E insert(E entity);

  E update(E entity);

  E reload(K id);

  boolean delete(E entity);

  List<E> getAll();
}
