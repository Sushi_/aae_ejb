package be.ipl.wazabi.dal.dao.impl;

import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;

import be.ipl.wazabi.core.FatalException;
import be.ipl.wazabi.dal.SqlErrors;
import be.ipl.wazabi.dal.dao.IPlayerDao;
import be.ipl.wazabi.dal.exception.ConstraintViolationException;
import be.ipl.wazabi.dal.exception.NonUniqueException;
import be.ipl.wazabi.entity.PlayerEntity;

@Stateless
public class PlayerDao extends BaseDao<Integer, PlayerEntity> implements IPlayerDao {

  private static final long serialVersionUID = -8500185836709571579L;

  public PlayerDao() {
    super(PlayerEntity.class);
  }

  public PlayerEntity getByUsername(String username) {
    return findOne("SELECT x FROM PlayerEntity x WHERE x.username = ?1", username);
  }

  /**
   * Converts SQL constraint violation exceptions to DaoException. DaoExceptions need to be
   * understood by the rest of the application regardless of the type of database used. (this is
   * because JEE persistence doesn't allow us to know the reason of their PersistenceException
   * easily)
   * 
   * @param e The exception to convert.
   * @return A DaoException explaining what went wrong.
   */
  protected ConstraintViolationException handleSqlConstraintViolation(SQLException e) {
    if (SqlErrors.UNIQUE_VIOLATION.equals(e.getSQLState())) {
      return new NonUniqueException("Username not unique", e);
    }

    return null;
  }

  @Override
  public PlayerEntity insertSafe(PlayerEntity entity) throws ConstraintViolationException {
    try {
      return super.insert(entity);
    } catch (PersistenceException originalException) {
      throw handlePersistenceException(originalException);
    }
  }

  @Override
  public PlayerEntity updateSafe(PlayerEntity entity) throws ConstraintViolationException {
    try {
      return super.update(entity);
    } catch (PersistenceException originalException) {
      throw handlePersistenceException(originalException);
    }
  }
  
  private RuntimeException handlePersistenceException(PersistenceException originalException)
      throws ConstraintViolationException {
    Throwable cause = originalException;
    while (!(cause instanceof SQLException)) {
      cause = cause.getCause();

      if (cause == null) {
        return originalException;
      }
    }

    ConstraintViolationException newException = handleSqlConstraintViolation((SQLException) cause);
    if (newException == null) {
      throw new FatalException("Received unexpected SQL exception", originalException);
    }

    throw newException;
  }
}
