package be.ipl.wazabi.dal.dao.impl;

import javax.ejb.Stateless;

import be.ipl.wazabi.dal.dao.IGamePlayerDao;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.entity.GamePlayerEntityPk;

@Stateless
public class GamePlayerDao extends BaseDao<GamePlayerEntityPk, GamePlayerEntity> implements IGamePlayerDao {

  private static final long serialVersionUID = -1965611513785118206L;

  public GamePlayerDao() {
    super(GamePlayerEntity.class);
  }
}
