package be.ipl.wazabi.dal.dao.impl;

import java.util.List;

import javax.ejb.Stateless;

import be.ipl.wazabi.dal.dao.IGameDao;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GameEntity.State;

@Stateless
public class GameDao extends BaseDao<Integer, GameEntity> implements IGameDao {

  private static final long serialVersionUID = -8193204264413835729L;

  public GameDao() {
    super(GameEntity.class);
  }

  @Override
  public GameEntity getPlayingGameForPlayer(int userId) {
    return findOne("SELECT game FROM GameEntity game "
        + "JOIN game.playerList AS player "
        + "JOIN player.player AS playerDetails "
        + "WHERE playerDetails.id = ?2 "
        + "AND game.state = ?1", State.PLAYING, userId);
  }

  @Override
  public GameEntity getCurrentGameForPlayer(int userId) {
    return findOne("SELECT game FROM GameEntity game "
        + "JOIN game.playerList AS player "
        + "JOIN player.player AS playerDetails "
        + "WHERE playerDetails.id = ?2 "
        + "AND game.state != ?1", State.ENDED, userId);
  }

  @Override
  public List<GameEntity> getNonFinishedGames() {
    return findMany("SELECT game FROM GameEntity game WHERE game.state != ?1", State.ENDED);
  }

  @Override
  public List<GameEntity> getGameByState(State state) {
    return findMany("SELECT game FROM GameEntity game WHERE game.state = ?1", state);
  }
}
