package be.ipl.wazabi.dal.dao.impl;

import javax.ejb.Stateless;

import be.ipl.wazabi.dal.dao.IDiceDao;
import be.ipl.wazabi.entity.DiceEntity;

@Stateless
public class DiceDao extends BaseDao<Integer, DiceEntity> implements IDiceDao {

  private static final long serialVersionUID = 4943128325783860939L;

  public DiceDao() {
    super(DiceEntity.class);
  }
}
