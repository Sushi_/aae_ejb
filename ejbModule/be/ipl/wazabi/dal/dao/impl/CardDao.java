package be.ipl.wazabi.dal.dao.impl;

import javax.ejb.Stateless;

import be.ipl.wazabi.dal.dao.ICardDao;
import be.ipl.wazabi.entity.CardEntity;

@Stateless
public class CardDao extends BaseDao<Integer, CardEntity> implements ICardDao {

  private static final long serialVersionUID = -3859942248682453744L;
  
  public CardDao() {
    super(CardEntity.class);
  }
}
