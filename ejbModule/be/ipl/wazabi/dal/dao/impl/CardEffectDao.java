package be.ipl.wazabi.dal.dao.impl;

import javax.ejb.Stateless;

import be.ipl.wazabi.dal.dao.ICardEffectDao;
import be.ipl.wazabi.entity.CardEffectEntity;

@Stateless
public class CardEffectDao extends BaseDao<Integer, CardEffectEntity> implements ICardEffectDao {

  private static final long serialVersionUID = -3859942248682453744L;
  
  public CardEffectDao() {
    super(CardEffectEntity.class);
  }
}
