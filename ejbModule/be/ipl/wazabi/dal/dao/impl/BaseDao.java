package be.ipl.wazabi.dal.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import be.ipl.wazabi.dal.dao.IBaseDao;

abstract class BaseDao<K, E> implements IBaseDao<K, E> {

  private static final long serialVersionUID = -6252347321324485016L;

  @PersistenceContext(unitName = "wazabidb")
  private EntityManager entityManager;

  private final Class<E> entityClass;

  public BaseDao(Class<E> entityClass) {
    this.entityClass = entityClass;
  }

  @Override
  public E getById(K id) {
    return (E) entityManager.find(entityClass, id);
  }

  @Override
  public E insert(E entity) {
    entityManager.persist(entity);
    return entity;
  }

  @Override
  public E update(E entity) {
    return entityManager.merge(entity);
  }

  @Override
  public E reload(K id) {
    E entity = getById(id);
    entityManager.refresh(entity);
    return entity;
  }

  @Override
  public boolean delete(E entity) {
    entityManager.remove(entity);
    return true;
  }

  @Override
  public List<E> getAll() {
    return findMany("SELECT x FROM " + entityClass.getName() + " x");
  }

  protected List<E> findMany(String queryString, Object... params) {
    try {
      return makeQuery(queryString, params).getResultList();
    } catch (NoResultException | NonUniqueResultException e) {
      return new ArrayList<>();
    }
  }

  protected E findOne(String queryString, Object... params) {
    try {
      return makeQuery(queryString, params).getSingleResult();
    } catch (NoResultException | NonUniqueResultException e) {
      return null;
    }
  }

  private TypedQuery<E> makeQuery(String jpqlQuery, Object... params) {
    TypedQuery<E> query = entityManager.createQuery(jpqlQuery, entityClass);

    int i = 0, j = 1;
    while (i < params.length) {
      if (params[i] instanceof Date) {
        query.setParameter(j, (Date) params[i], (TemporalType) params[i + 1]);
        i += 2;
      } else if (params[i] instanceof Calendar) {
        query.setParameter(j, (Calendar) params[i], (TemporalType) params[i + 1]);
        i += 2;
      } else {
        query.setParameter(j, params[i]);
        i++;
      }

      j++;
    }

    return query;
  }
}
