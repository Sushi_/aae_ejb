package be.ipl.wazabi.dal.dao;

import java.util.List;

import javax.ejb.Local;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GameEntity.State;

@Local
public interface IGameDao extends IBaseDao<Integer, GameEntity> {

  /**
   * Searches for a game where the player is active.
   * 
   * @param userId The id of the player.
   * @return The game entity where the player is active, or null if no game is found.
   */
  GameEntity getPlayingGameForPlayer(int userId);

  /**
   * Searches for a game where the player is waiting or playing.
   * 
   * @param userId The id of the player.
   * @return The game entity where the player is waiting or playing, or null if no game is found.
   */
  GameEntity getCurrentGameForPlayer(int userId);
  
  /**
   * Returns the list of games waiting for players or currently playing.
   */
  List<GameEntity> getNonFinishedGames();
  
  /**
   * Returns the list of games matching a given state.
   * @param State the state to match.
   */
  List<GameEntity> getGameByState(State state);
}
