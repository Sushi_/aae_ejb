package be.ipl.wazabi.dal.dao;

import javax.ejb.Local;

import be.ipl.wazabi.entity.DiceEntity;

@Local
public interface IDiceDao extends IBaseDao<Integer, DiceEntity> {

}
