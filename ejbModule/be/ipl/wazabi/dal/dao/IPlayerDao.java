package be.ipl.wazabi.dal.dao;

import javax.ejb.Local;

import be.ipl.wazabi.dal.exception.ConstraintViolationException;
import be.ipl.wazabi.dal.exception.NonUniqueException;
import be.ipl.wazabi.entity.PlayerEntity;

@Local
public interface IPlayerDao extends IBaseDao<Integer, PlayerEntity> {

  @Deprecated
  PlayerEntity insert(PlayerEntity entity);

  @Deprecated
  PlayerEntity update(PlayerEntity entity);

  /**
   * Inserts a player entity in the database.
   * @throws NonUniqueException The username is not unique.
   */
  PlayerEntity insertSafe(PlayerEntity entity) throws ConstraintViolationException;

  /**
   * Updates a player entity in the database.
   * @throws NonUniqueException The username is not unique.
   */
  PlayerEntity updateSafe(PlayerEntity entity) throws ConstraintViolationException;

  /**
   * Returns the player matching a given username if it exists. Null otherwise
   * @param username The player's username.
   */
  PlayerEntity getByUsername(String username);
}
