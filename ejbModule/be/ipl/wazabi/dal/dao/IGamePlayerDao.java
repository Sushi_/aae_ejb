package be.ipl.wazabi.dal.dao;

import javax.ejb.Local;

import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.entity.GamePlayerEntityPk;

@Local
public interface IGamePlayerDao extends IBaseDao<GamePlayerEntityPk, GamePlayerEntity> {

}
