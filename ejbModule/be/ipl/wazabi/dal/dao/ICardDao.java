package be.ipl.wazabi.dal.dao;

import javax.ejb.Local;

import be.ipl.wazabi.entity.CardEntity;

@Local
public interface ICardDao extends IBaseDao<Integer, CardEntity> {

}
