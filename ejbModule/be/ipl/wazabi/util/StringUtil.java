package be.ipl.wazabi.util;

import java.util.regex.Pattern;

/**
 * Utility methods callable for strings.
 *
 * @author Guylian Cox
 */
public class StringUtil {

  private static final Pattern NUM_REGEX = Pattern.compile("^[0-9]*$");
  private static final Pattern ALPHA_REGEX = Pattern.compile("^[a-zA-Z]*$");
  private static final Pattern ALPHANUM_REGEX = Pattern.compile("^[a-zA-Z0-9]*$");
  private static final Pattern NONEMPTY_REGEX = Pattern.compile("\\S+");
  private static final Pattern EMAIL_REGEX =
      Pattern.compile("^([a-zA-Z0-9][+a-zA-Z0-9_.-]*)+@([a-zA-Z0-9][a-zA-Z0-9_.-]*)+\\.[a-zA-Z]*$");

  /**
   * Verifies if a string contains only alphabetical letters.
   *
   * @param str The string to check.
   * @return true if the string contains only letters, false otherwise.
   */
  public static boolean isAlphaString(String str) {
    return ALPHA_REGEX.matcher(str).matches();
  }

  /**
   * Verifies if a string contains only alphabetical letters and numbers.
   *
   * @param str The string to check.
   * @return true if the string contains only letters and numbers, false otherwise.
   */
  public static boolean isAlphanumeric(String str) {
    return ALPHANUM_REGEX.matcher(str).matches();
  }

  /**
   * Verifies if a string contains only numbers
   *
   * @param str The string to check.
   * @return true if the string contains only numbers, false otherwise.
   */
  public static boolean isNumeric(String str) {
    return NUM_REGEX.matcher(str).matches();
  }

  /**
   * Verifies if a string is following a valid email format.
   *
   * @param str The string to check.
   * @return true if the string is an email, false otherwise.
   */
  public static boolean isEmail(String str) {
    return EMAIL_REGEX.matcher(str).matches();
  }

  /**
   * Verifies if a string is following a valid password format according to the application
   * specifications.
   *
   * @param str The string to check.
   * @return true if the string is a password, false otherwise.
   */
  public static boolean isValidPassword(String str) {
    // Implement something to ensure a high entropy ?
    // http://en.wikipedia.org/wiki/Password_strength
    //return str != null && str.length() >= 6;
    
    return !isEmpty(str);
  }

  /**
   * Checks if a string contains non-space characters.
   *
   * @param str The string to check.
   * @return true if the string contains non-space characters, false otherwise.
   */
  public static boolean isEmpty(String str) {
    return str == null || !NONEMPTY_REGEX.matcher(str).find();
  }

  /**
   * Converts a string containing hexadecimal numbers to a byte array.
   *
   * @param hexStr The string to convert
   * @return the byte array version
   */
  public static byte[] strToBytes(String hexStr) {
    byte[] table = new byte[hexStr.length() >> 1];

    for (int i = hexStr.length() - 1; i > 0; i -= 2) {
      int hexValue = Integer.parseInt(hexStr.substring(i - 1, i + 1), 16);

      table[i >> 1] = (byte) hexValue;
    }

    return table;
  }

  /**
   * Converts an array of bytes to an string of hexadecimal numbers.
   *
   * @param hexTab The table to stringify
   * @return A string representation of the table
   */
  public static String bytesToString(byte[] hexTab) {
    StringBuilder str = new StringBuilder(hexTab.length * 2);

    for (byte b : hexTab) {
      String hexRepresentation = Integer.toHexString(b & 0xFF);

      if (hexRepresentation.length() == 1) {
        str.append('0');
      }

      str.append(hexRepresentation);
    }

    return str.toString();
  }
}
