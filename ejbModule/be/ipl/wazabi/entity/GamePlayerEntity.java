package be.ipl.wazabi.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(GamePlayerEntityPk.class)
@Table(name = "gameplayers", schema = "wazabi")
public class GamePlayerEntity implements Serializable {

  private static final long serialVersionUID = 4645322093880029447L;

  public enum State {
    WAITING, THROWING_DICES, GIVING_DICES, USING_CARD;
  }

  @Id
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "player_id")
  private PlayerEntity player;

  @Id
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "game_id")
  private GameEntity game;

  @Column(name = "play_order")
  private int playOrder;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "user_dices", schema = "wazabi",
      joinColumns = {@JoinColumn(name = "player_id", referencedColumnName = "player_id"),
          @JoinColumn(name = "game_id", referencedColumnName = "game_id")},
      inverseJoinColumns = @JoinColumn(name = "dice_id", referencedColumnName = "id") )
  private Set<DiceEntity> playableDices;
  // @MapKey
  // private Map<Integer, DiceEntity> playableDices;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "user_cards", schema = "wazabi",
      joinColumns = {@JoinColumn(name = "player_id", referencedColumnName = "player_id"),
          @JoinColumn(name = "game_id", referencedColumnName = "game_id")},
      inverseJoinColumns = @JoinColumn(name = "card_id", referencedColumnName = "id") )
  private Set<CardEntity> playableCards;
  // @MapKey
  // private Map<Integer, CardEntity> playableCards;

  @Column(name = "turns_to_skip")
  private int turnsToSkipCount;

  @NotNull
  @Enumerated
  private State state;

  protected GamePlayerEntity() {}

  public GamePlayerEntity(PlayerEntity player, GameEntity game) {
    this.player = player;
    this.game = game;
    this.turnsToSkipCount = 0;
    this.state = State.WAITING;

    this.playableCards = new HashSet<>();
    this.playableDices = new HashSet<>();
  }

  public boolean shouldSkipTurn() {
    if (turnsToSkipCount > 0) {
      turnsToSkipCount--;
      return true;
    }

    return false;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public void makeSkipNextTurn() {
    this.turnsToSkipCount++;
  }

  public PlayerEntity getPlayer() {
    return player;
  }

  public GameEntity getGame() {
    return game;
  }

  void setPlayOrder(int playOrder) {
    this.playOrder = playOrder;
  }

  public int getPlayOrder() {
    return playOrder;
  }

  // DICES
  public Set<DiceEntity> getPlayableDices() {
    return Collections.unmodifiableSet(playableDices);
  }

  public Set<DiceEntity> removeAllDices() {
    Set<DiceEntity> dices = getPlayableDices();
    this.playableDices = new HashSet<>();

    return dices;
  }

  public DiceEntity removeDice(DiceEntity dice) {

    if (this.playableDices.remove(dice) && this.playableDices.size() == 0) {
      this.getGame().setWinner(this);
    }

    return dice;
  }

  public DiceEntity removeDice() {
    if (this.playableDices.isEmpty()) {
      return null;
    }

    DiceEntity dice = playableDices.iterator().next();
    playableDices.remove(dice);

    if (this.playableDices.isEmpty()) {
      this.game.setWinner(this);
    }

    return dice;
  }

  public boolean addDice(DiceEntity dice) {
    if (dice == null) {
      return false;
    }

    playableDices.add(dice);
    // playableDices.put(dice.getId(), dice);
    return true;
  }

  // CARDS
  public Set<CardEntity> getPlayableCards() {
    return Collections.unmodifiableSet(playableCards);
    // return playableCards.values();
  }

  public CardEntity removeCardFromDeck(int cardId) {

    for (CardEntity card : playableCards) {
      if (card.getId() == cardId) {
        playableCards.remove(card);
        return card;
      }
    }

    return null;
    // return playableCards.remove(cardId);
  }

  public boolean addCardToDeck(CardEntity card) {
    if (card == null) {
      return false;
    }

    // playableCards.put(card.getId(), card);
    playableCards.add(card);
    return true;
  }

  public Set<CardEntity> removeAllCards() {
    Set<CardEntity> cards = getPlayableCards();
    this.playableCards = new HashSet<>();

    return cards;
  }


  public int countDicesOfType(int type) {
    int val = 0;

    for (DiceEntity dice : playableDices) {
      if (dice.getValue() == type) {
        val++;
      }
    }

    return val;
  }
  
  CardEntity removeRandomCard() {
    if (playableCards.isEmpty()) {
      return null;
    }

    return playableCards.iterator().next();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((game == null) ? 0 : game.hashCode());
    result = prime * result + ((player == null) ? 0 : player.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    GamePlayerEntity other = (GamePlayerEntity) obj;
    if (game == null) {
      if (other.game != null)
        return false;
    } else if (!game.equals(other.game))
      return false;
    if (player == null) {
      if (other.player != null)
        return false;
    } else if (!player.equals(other.player))
      return false;
    return true;
  }
}
