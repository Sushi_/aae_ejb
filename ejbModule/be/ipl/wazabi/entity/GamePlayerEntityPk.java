package be.ipl.wazabi.entity;

import java.io.Serializable;

public class GamePlayerEntityPk implements Serializable {

  private static final long serialVersionUID = -7398422766034176507L;

  private int player;

  private int game;

  protected GamePlayerEntityPk() {}

  public GamePlayerEntityPk(int playerId, int gameId) {
    this.player = playerId;
    this.game = gameId;
  }

  public int getPlayerId() {
    return player;
  }

  public int getGameId() {
    return game;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + game;
    result = prime * result + player;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    GamePlayerEntityPk other = (GamePlayerEntityPk) obj;
    if (game != other.game)
      return false;
    if (player != other.player)
      return false;
    return true;
  }
}
