package be.ipl.wazabi.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cards", schema = "wazabi")
public class CardEntity implements Serializable {

  private static final long serialVersionUID = -7427325131658883323L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  
  @NotNull
  @ManyToOne
  @JoinColumn(name = "effect_code")
  private CardEffectEntity effect;
  
  public CardEntity(CardEffectEntity effect) {
    this.effect = effect;
  }

  protected CardEntity() {}

  public int getId() {
    return id;
  }

  public CardEffectEntity getEffect() {
    return effect;
  }

  @Override
  public String toString() {
    return "CardEntity [id=" + id + ", effect=" + effect + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CardEntity other = (CardEntity) obj;
    if (id != other.id)
      return false;
    return true;
  }
}