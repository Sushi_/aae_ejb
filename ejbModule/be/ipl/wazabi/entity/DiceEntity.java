package be.ipl.wazabi.entity;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import be.ipl.wazabi.main.Setup;

@Entity
@Table(name = "dices", schema = "wazabi")
public class DiceEntity implements Serializable {

  private static final long serialVersionUID = -7537328501438669744L;

  public static final int WAZABI_FACE = 1;
  public static final int DRAW_CARD_FACE = 2;
  public static final int GIVE_DICE_FACE = 3;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Min(1)
  @Max(3)
  private int value;

  @Transient
  private final Random random = new Random();

  public DiceEntity() {
    value = 1;
  }

  public int getId() {
    return id;
  }

  public int getValue() {
    return value;
  }

  public int roll(Setup configuration) {
    int totalFaces = configuration.getFaceCountCard() + configuration.getFaceCountDice()
        + configuration.getFaceCountWazabi();

    int randomResult = random.nextInt(totalFaces) + 1;

    if (randomResult > configuration.getFaceCountCard() + configuration.getFaceCountCard()) {
      return value = DiceEntity.GIVE_DICE_FACE;
    } else if (randomResult > configuration.getFaceCountCard())
      return value = DiceEntity.DRAW_CARD_FACE;
    else {
      return value = DiceEntity.WAZABI_FACE;
    }
  }
}


