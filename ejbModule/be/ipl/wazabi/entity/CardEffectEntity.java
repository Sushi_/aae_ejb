package be.ipl.wazabi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "card_effects", schema = "wazabi")
public class CardEffectEntity implements Serializable {

  private static final long serialVersionUID = 6483553969961484109L;

  @Id
  @Column(name = "effect_code")
  private int effectCode;
  
  @NotNull
  private String name;
  
  @NotNull
  private String description;
  
  @Min(0)
  @Max(3)
  @NotNull
  @Column(name = "use_cost")
  private int useCost;

  protected CardEffectEntity() {}
  
  public CardEffectEntity(int effectCode, int useCost, String name, String description) {
    this.effectCode = effectCode;
    this.name = name;
    this.description = description;
    this.useCost = useCost;
  }

  public int getCode() {
    return effectCode;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public int getUseCost() {
    return useCost;
  }

  @Override
  public String toString() {
    return "CardEffectEntity [effectCode=" + effectCode + ", name=" + name + ", description="
        + description + ", useCost=" + useCost + "]";
  }
}
