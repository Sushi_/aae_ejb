package be.ipl.wazabi.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "games", schema = "wazabi")
public class GameEntity implements Serializable {

  private static final long serialVersionUID = 7522067499278876513L;

  public static final int DICES_COUNT = 24;
  public static final int CLOCKWISE_DIRECTION = 1;
  public static final int ANTI_CLOCKWISE_DIRECTION = -1;

  public enum State {
    INITIATING {
      @Override
      void nextState(GameEntity game) {
        game.state = PLAYING;
        
        // randomize play order
        Collections.shuffle(game.playerList);
        List<GamePlayerEntity> players = game.playerList;
        for (int i = 0; i < players.size(); i++) {
          players.get(i).setPlayOrder(i);
        }

        game.currentPlayer = game.playerList.get(0);
        game.currentPlayer.setState(GamePlayerEntity.State.THROWING_DICES);
      }
    },

    PLAYING {
      @Override
      void nextState(GameEntity game) {
        game.state = ENDED;
      }

      @Override
      GamePlayerEntity removePlayer(GameEntity game, PlayerEntity player) {
        GamePlayerEntity removedPlayer = super.removePlayer(game, player);

        if (game.playerList.size() == 1) {
          // end the game if everybody except 1 player gave up.
          game.setWinner(game.playerList.get(0));
        }

        return removedPlayer;
      }
    },

    ENDED {
      @Override
      void nextState(GameEntity game) {
        throw new IllegalStateException("Game already in the final state");
      }

      @Override
      GamePlayerEntity removePlayer(GameEntity game, PlayerEntity player) {
        throw new IllegalStateException("Cannot remove a player when the game is ended");
      }
    };

    abstract void nextState(GameEntity game);

    GamePlayerEntity removePlayer(GameEntity game, PlayerEntity player) {
      if (player.equals(game.currentPlayer)) {
        game.nextPlayer();
      }

      GamePlayerEntity gamePlayerInstance = game.getPlayerInstance(player);
      if (gamePlayerInstance == null) {
        return null;
      }

      if (!game.playerList.remove(gamePlayerInstance)) {
        return null;
      }

      return gamePlayerInstance;
    }
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @NotNull
  private String name;

  @NotNull
  @Temporal(TemporalType.DATE)
  @Column(name = "creation_date")
  private Calendar creationDate;

  @NotNull
  private int direction;

  @NotNull
  @Enumerated
  private State state;

  @ManyToMany
  @JoinTable(name = "game_cards", schema = "wazabi", joinColumns = @JoinColumn(name = "game_id") ,
      inverseJoinColumns = @JoinColumn(name = "card_id") )
  private List<CardEntity> deckCard;

  @OneToMany(mappedBy = "game", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @OrderBy("playOrder")
  private List<GamePlayerEntity> playerList;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumns({@JoinColumn(name = "current_player_id"), @JoinColumn(name = "current_game_id")})
  private GamePlayerEntity currentPlayer;

  @ManyToOne
  @JoinColumn(name = "winner_id")
  private PlayerEntity winner;

  @Transient
  private final Random random = new Random();

  protected GameEntity() {}

  public GameEntity(String name) {
    this.name = name;
    this.direction = GameEntity.CLOCKWISE_DIRECTION;
    this.state = State.INITIATING;
    this.creationDate = new GregorianCalendar();

    this.playerList = new ArrayList<>();
    this.deckCard = new ArrayList<>();
  }

  public State getState() {
    return state;
  }

  /**
   * Moves the game on to the next state, state order is {@link State#INITIATING},
   * {@link State#PLAYING}, and {@link State#ENDED}.
   * 
   * @throws IllegalStateException The game is already on its final state.
   */
  public void nextState() {
    state.nextState(this);
  }

  /**
   * Add a player to the list of player, starts the game if there is {@link GameEntity#MIN_PLAYERS}
   * players in the game.
   * 
   * @param player The player to add.
   * @return The game specific player instance.
   */
  public GamePlayerEntity addPlayer(PlayerEntity player) {
    if (state != State.INITIATING) {
      throw new IllegalStateException();
    }

    GamePlayerEntity tempPlayer = new GamePlayerEntity(player, this);
    playerList.add(tempPlayer);

    return tempPlayer;
  }

  /**
   * Removes a player from the game.
   * 
   * @param user The player to remove
   * @return instance of player or null.
   * 
   * @throws IllegalStateException game ended, cannot edit players anymore.
   */
  public GamePlayerEntity removePlayer(PlayerEntity user) {
    return state.removePlayer(this, user);
  }

  public GamePlayerEntity getCurrentPlayer() {
    return currentPlayer;
  }

  /**
   * Returns the game-specific instance of a player if they are part of the game.
   * 
   * @param player The player for which to fetch the game-specific instance.
   */
  public GamePlayerEntity getPlayerInstance(PlayerEntity player) {
    return getPlayerInstance(player.getId());
  }

  public GamePlayerEntity getPlayerInstance(int playerId) {
    for (GamePlayerEntity playerInstance : playerList) {
      if (playerInstance.getPlayer().getId() == playerId) {
        return playerInstance;
      }
    }

    return null;
  }

  /**
   * Inverts the player play order.
   */
  public void invertDirection() {
    this.direction *= -1;
  }

  /**
   * Marks the next player as the one playing following the current play order.
   * 
   * @return The next player playing.
   */
  public GamePlayerEntity nextPlayer() {
    if (state != State.PLAYING) {
      throw new IllegalStateException();
    }

    currentPlayer.setState(GamePlayerEntity.State.WAITING);

    do {
      calcNextPlayer();
    } while (currentPlayer.shouldSkipTurn());

    currentPlayer.setState(GamePlayerEntity.State.THROWING_DICES);

    return currentPlayer;
  }

  private void calcNextPlayer() {
    int index = currentPlayer.getPlayOrder();
    int nextIndex = (index + direction);

    if (direction == CLOCKWISE_DIRECTION) {
      nextIndex %= playerList.size();
    } else if (nextIndex < 0) {
      nextIndex = playerList.size() - 1;
    }

    this.currentPlayer = playerList.get(nextIndex);
  }

  /**
   * Returns the instance of the winning player.
   */
  public PlayerEntity getWinner() {
    return winner;
  }

  /**
   * Marks a given player as the winner.
   * 
   * @param gamePlayerEntity The winning player.
   *
   * @throws IllegalArgumentException The player is not part of this game.
   * @throws IllegalArgumentException The player does not meet the winning conditions (having no
   *         dices).
   * @throws IllegalStateException The game is not in a playing state.
   */
  public void setWinner(GamePlayerEntity gamePlayerEntity) {
    if (state != State.PLAYING) {
      throw new IllegalStateException("Game is not playing");
    }

    if (!this.equals(gamePlayerEntity.getGame())) {
      throw new IllegalArgumentException("That player is not from this game.");
    }

    if (this.playerList.size() != 1 && !gamePlayerEntity.getPlayableDices().isEmpty()) {
      throw new IllegalArgumentException("This player cannot win, they still have dices.");
    }

    this.winner = gamePlayerEntity.getPlayer();
    this.nextState();
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public Calendar getCreationDate() {
    return creationDate;
  }

  public List<GamePlayerEntity> getPlayerList() {
    return Collections.unmodifiableList(playerList);
  }

  public int getPlayerCount() {
    return playerList.size();
  }

  /**
   * Removes a random card from the game deck.
   * 
   * @return The card.
   */
  public CardEntity drawCard() {
    if (deckCard.size() == 0) {
      return stealCardFromPlayer();
    }

    return deckCard.remove(random.nextInt(deckCard.size()));
  }
  
  private CardEntity stealCardFromPlayer() {
    for (GamePlayerEntity otherPlayer : playerList) {
      if (otherPlayer.getPlayer().getId() == currentPlayer.getPlayer().getId()) {
        continue;
      }
      
      CardEntity card = otherPlayer.removeRandomCard();
      if (card != null) {
        return card;
      }
    }
    
    return null;
  }

  public boolean addCardToDeck(CardEntity card) {
    return deckCard.add(card);
  }
}
