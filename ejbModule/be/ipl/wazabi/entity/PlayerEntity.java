package be.ipl.wazabi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "players", schema = "wazabi")
public class PlayerEntity implements Serializable {

  private static final long serialVersionUID = -7427325131658883323L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @NotNull
  @Column(unique = true)
  private String username;

  @NotNull
  @Column()
  private String password;

  public PlayerEntity(String username, String password) {
    this.username = username;
    this.password = password;
  }

  protected PlayerEntity() {}

  public int getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PlayerEntity other = (PlayerEntity) obj;
    if (id != other.id)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "PlayerEntity [id=" + id + ", username=" + username + "]";
  }

  public void setPassword(String hashPassword) {
    this.password = hashPassword;
  }
}
