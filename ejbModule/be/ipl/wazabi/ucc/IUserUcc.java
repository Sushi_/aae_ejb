package be.ipl.wazabi.ucc;

import javax.ejb.Remote;

import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

@Remote
/**
 * UCCs related to players.
 */
public interface IUserUcc {
  
  int ERRNO_LOGIN_INVALID_USERNAME = 1;
  int ERRNO_LOGIN_INVALID_PASSWORD = 2;
  
  int ERRNO_REGISTER_INVALID_USERNAME = 1;
  int ERRNO_REGISTER_INVALID_PASSWORD = 2;
  int ERRNO_REGISTER_DUPLICATE_USERNAME = 3;
  
  /**
   * Logs a player in.
   *
   * @param username The username of the player.
   * @param password The password of the player's account.
   * @return The entity of the logged player if the credentials are valid.
   * 
   * @throws UccException errno {@link ERRNO_LOGIN_INVALID_USERNAME} - Invalid username.
   * @throws UccException errno {@link ERRNO_LOGIN_INVALID_PASSWORD} - Invalid password.
   */
  PlayerEntity logIn(String username, String password) throws UccException;

  /**
   * Adds a user to the list of users.
   *
   * @param username 
   * @param password
   * @return The registered entity.
   * 
   * @throws UccException errno {@link ERRNO_REGISTER_INVALID_USERNAME} - Invalid username.
   * @throws UccException errno {@link ERRNO_REGISTER_DUPLICATE_USERNAME} - Username already in use.
   * @throws UccException errno {@link ERRNO_REGISTER_INVALID_PASSWORD} - Invalid password.
   */
  PlayerEntity register(String username, String password) throws UccException;
  
  /**
   * Returns the player with a given ID or null if not found.
   * @param id the id of the player.
   */
  PlayerEntity getById(int id);
}
