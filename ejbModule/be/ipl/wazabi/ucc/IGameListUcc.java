package be.ipl.wazabi.ucc;

import java.util.List;

import javax.ejb.Remote;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.ucc.exception.UccException;

@Remote
/**
 * UCC Used for listing games.
 */
public interface IGameListUcc {
  
  int ERRNO_JOIN_INVALID_GAMESTATE = 1;
  int ERRNO_JOIN_INVALID_PLAYERSTATE = 2;
  
  int ERRNO_CREATE_INVALID_NAME = 3;
  
  /**
   * Returns the list of games matching a given state.
   */
  List<GameEntity> listGamesByState(GameEntity.State state);
  
  /**
   * Returns the list of games matching that haven't ended yet.
   */
  List<GameEntity> listActiveGames();
  
  
  /**
   * Adds a player in a game if it exists.
   * If the count of player in the game is {@link GameEntity#MIN_PLAYERS}, start the game and give the dices to every player.
   * 
   * @param gameId The id of the game to join.
   * @param playerId The id of the player who whishes to join the game.
   * @return The joined game.
   * 
   * @throws UccException errno {@link ERRNO_JOIN_INVALID_STATE} - The game is not waiting for any more players or it does not exist.
   * @throws UccException errno {@link ERRNO_JOIN_INVALID_PLAYERSTATE} - The player is already in a game or he does not exist.
   */
  GameEntity joinGame(int gameId, int playerId) throws UccException;
  
  /**
   * Creates a new game.
   * @param gameName The name of the game. Cannot be null nor empty.
   * @return The created game.
   * 
   * @throws UccException errno {@link ERRNO_CREATE_INVALID_NAME} - The game name is invalid.
   */
  GameEntity createGame(String gameName) throws UccException;

  /**
   * Returns the game in which a given player is or null if he doesn't exist or isn't in any game.
   * @param playerId The id of the player to lookup.
   */
  GameEntity getCurrentGame(int playerId);
  
  /**
   * Returns the game with a given id or null if none matched.
   * @param id The id of the game to fetch.
   */
  GameEntity getGameById(int id);
}
