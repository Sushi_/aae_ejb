package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

class PickupCardsLogic extends BaseCardLogic {
  
  private static final int CARD_COUNT = 3;

  public PickupCardsLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 7;
  }

  /**
   * Add 3 cards from the game deck to the player's deck.
   */
  @Override
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    GameEntity game = player.getGame();
    
    for (int i = 0; i < CARD_COUNT; i++) {
      CardEntity card = game.drawCard();
      player.addCardToDeck(card);
    }
  }
}
