package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

class GiveDiceLogic extends BaseCardLogic {

//  @EJB
//  private IUserUcc userUcc;

  public GiveDiceLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 4;
  }

  @Override
  /**
   * Transfers a dice from the user to another player. Additional parameters: the id of the player
   * to give the dice to.
   */
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    if (parameters.length != 1) {
      throw new UccException("[GiveDice] Invalid parameter count", IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    GamePlayerEntity otherPlayer = LogicUtil.getGamePlayer(parameters[0], player.getGame());
    if (otherPlayer == null) {
      throw new UccException("[GiveDice] Player " + parameters[0] + " is not part of the game",
          IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    otherPlayer.addDice(player.removeDice());
  }
}
