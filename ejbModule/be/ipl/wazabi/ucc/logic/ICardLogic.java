package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.core.FatalException;
import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

public interface ICardLogic {

  /**
   * Use a card.
   * 
   * @param card The card to use.
   * @param player The user using the card.
   * @param nbCurseDices The count of {@link DiceEntity#WAZABI_FACE} dices the player rolled.
   * @param parameters Various parameters which depend on the effectId
   * 
   * @throws UccException errno {@link IGameUcc#ERRNO_NOT_ENOUGH_DICES} - The player doesn't have
   *         enough {@link DiceEntity#WAZABI_FACE} dices to play this card.
   * @throws FatalException The effectId is unknown.
   */
  void useCard(CardEntity card, GamePlayerEntity player, String... parameters)
      throws UccException;
}
