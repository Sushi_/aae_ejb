package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.core.FatalException;
import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

abstract class BaseCardLogic implements ICardLogic {

  private final ICardLogic nextLogic;

  public BaseCardLogic(ICardLogic nextlogic) {
    this.nextLogic = nextlogic;
  }

  public final void useCard(CardEntity card, GamePlayerEntity player, String... parameters) throws UccException {
    if (getHandledEffect() == card.getEffect().getCode()) {
      handleCard(player, parameters);
      return;
    }

    if (nextLogic == null) {
      throw new FatalException("Unknown effect id " + card.getEffect().getCode());
    }

    nextLogic.useCard(card, player, parameters);
  }
  
  public abstract int getHandledEffect();

  public abstract void handleCard(GamePlayerEntity player, String... parameters) throws UccException;
}
