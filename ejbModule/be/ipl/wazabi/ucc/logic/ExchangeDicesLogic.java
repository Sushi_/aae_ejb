package be.ipl.wazabi.ucc.logic;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

class ExchangeDicesLogic extends BaseCardLogic {

  public ExchangeDicesLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 2;
  }

  @Override
  /**
   * Every player gives their dice to the player on the right or on the left. Additional parameters:
   * "clockwise" or "anticlockwise" depending on the direction in which the dice must be
   * redistributed.
   * 
   * @throws UccException errno {@link IGameUcc#ERRNO_CANNOT_USE_CARD} - Cannot use this card.
   */
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    if (parameters.length != 1) {
      throw new UccException("[ExchangeDices] Invalid parameter count", IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    int direction = "clockwise".equals(parameters[0])
        ? GameEntity.CLOCKWISE_DIRECTION
        : GameEntity.ANTI_CLOCKWISE_DIRECTION;

    GameEntity game = player.getGame();
    List<GamePlayerEntity> players = game.getPlayerList();

    GamePlayerEntity currentReceiver = players.get(0);
    Collection<DiceEntity> firstReceiversDices = currentReceiver.removeAllDices();

    for (int i = 0; i < players.size() - 1; i++) {
      int giverPos = getGiverPos(i, players.size(), direction);
      GamePlayerEntity giver = players.get(giverPos);

      giveDices(giver.removeAllDices(), currentReceiver);

      currentReceiver = giver;
    }

    giveDices(firstReceiversDices, currentReceiver);
  }

  private void giveDices(Collection<DiceEntity> dices, GamePlayerEntity receiver) {
    Iterator<DiceEntity> giverDices = dices.iterator();
    while (giverDices.hasNext()) {
      receiver.addDice(giverDices.next());
    }
  }

  private int getGiverPos(int pos, int size, int direction) {
    int next = pos - direction;
    if (next >= size) {
      next = 0;
    } else if (next < 0) {
      next = size - 1;
    }

    return next;
  }
}
