package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.util.StringUtil;

public final class LogicUtil {

  public static GamePlayerEntity getGamePlayer(String playeridStr, GameEntity game) {
    try {
      if (!StringUtil.isNumeric(playeridStr)) {
        return null;
      }

      int playerId = Integer.parseInt(playeridStr);

      return game.getPlayerInstance(playerId);
    } catch (NumberFormatException ignore) {
      return null;
    }
  }
}
