package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

class SkipTurnLogic extends BaseCardLogic {
  
  public SkipTurnLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 9;
  }

  @Override
  /**
   * Make a user skip a turn.
   * Additional parameters: the user made skipping a turn.
   */
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    if (parameters.length != 1) {
      throw new UccException("[SkipTurn] Invalid parameter count", IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    GamePlayerEntity otherPlayer = LogicUtil.getGamePlayer(parameters[0], player.getGame());
    if (otherPlayer == null) {
      throw new UccException("[SkipTurn] Player " + parameters[0] + " is not part of the game", IGameUcc.ERRNO_INVALID_PARAMETERS);
    }
    
    otherPlayer.makeSkipNextTurn();
  }
}
