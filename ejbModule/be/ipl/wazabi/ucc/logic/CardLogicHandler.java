package be.ipl.wazabi.ucc.logic;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

/**
 * Can't inject a chain of responsibilities, let's inject this instead.
 */
@LocalBean
@Singleton
public class CardLogicHandler implements ICardLogic {

  private final ICardLogic next;
  
  public CardLogicHandler() {
    next = new DiscardOneDiceLogic(
      new DiscardTwoDicesLogic(
        new ExchangeDicesLogic(
          new GiveDiceLogic(
            new InvertDirectionLogic(
              new PickupCardsLogic(
                new RemoveAllPlayersCardsLogic(
                  new RemovePlayerCardsLogic(
                    new SkipTurnLogic(
                      new TakeCardLogic(null))))))))));
  }

  @Override
  public void useCard(CardEntity card, GamePlayerEntity player, String... parameters)
      throws UccException {
    try {
      next.useCard(card, player, parameters);
    } catch (UccException e) {
      throw new UccException(e.getMessage() + " [debug: cardid = " + card.getId() + ", cardeffect = " + card.getEffect().getCode() + "]", e.getErrno(), e);
    }
  }
}
