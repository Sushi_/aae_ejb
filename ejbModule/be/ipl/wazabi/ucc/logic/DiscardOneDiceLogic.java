package be.ipl.wazabi.ucc.logic;

import java.util.Collection;

import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

class DiscardOneDiceLogic extends BaseCardLogic {

  public DiscardOneDiceLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 1;
  }

  /**
   * Removes 1 dices from the player's list. Cannot use this card if the player rolled 2 or more
   * {@link DiceEntity#GIVE_DICE_FACE} prior to playing this card.
   * 
   * @throws UccException errno {@link IGameUcc#ERRNO_CANNOT_USE_CARD} - Cannot use this card.
   */
  @Override
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    Collection<DiceEntity> dices = player.getPlayableDices();

    int dropDiceCount = 0;
    for (DiceEntity dice : dices) {
      if (dice.getValue() == DiceEntity.GIVE_DICE_FACE) {
        dropDiceCount++;
      }
    }

    if (dropDiceCount >= 2) {
      throw new UccException("[DiscardOneDice] Cannot use this card, had 2 or more discard dices",
          IGameUcc.ERRNO_CANNOT_USE_CARD);
    }

    player.removeDice();
  }
}
