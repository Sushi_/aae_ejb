package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

class DiscardTwoDicesLogic extends BaseCardLogic {

  public DiscardTwoDicesLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 3;
  }

  @Override
  /**
   * Removes 2 dices from the player's list.
   */
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    player.removeDice();
    player.removeDice();
  }
}
