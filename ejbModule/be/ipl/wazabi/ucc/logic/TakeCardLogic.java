package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

class TakeCardLogic extends BaseCardLogic {

  public TakeCardLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 5;
  }

  /**
   * Take a card from another player and adds it to the user's deck.
   * Additional parameters: Other user's id, card id.
   */
  @Override
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    if (parameters.length != 2) {
      throw new UccException("[TakeCard] Invalid parameter count, requires 2, got " + parameters.length, IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    GamePlayerEntity otherPlayer = LogicUtil.getGamePlayer(parameters[0], player.getGame());
    if (otherPlayer == null) {
      throw new UccException("[TakeCard] Player " + parameters[0] + " is not part of the game", IGameUcc.ERRNO_INVALID_PARAMETERS);
    }
    
    CardEntity card = null;
    try {
      int cardid = Integer.parseInt(parameters[1]);
      card = otherPlayer.removeCardFromDeck(cardid);
    } catch (NumberFormatException e) {}
    
    if (card == null) {
      throw new UccException("[TakeCard] Player " + parameters[0] + " does not have a card with id " + parameters[1], IGameUcc.ERRNO_INVALID_PARAMETERS);
    }
    
    player.addCardToDeck(card);
  }
}
