package be.ipl.wazabi.ucc.logic;

import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

class InvertDirectionLogic extends BaseCardLogic {
  
  public InvertDirectionLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 10;
  }

  @Override
  /**
   * Invert the game direction and make the user play again.
   */
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    GameEntity game = player.getGame();

    /* as the direction is inverted, it will be the previous player which is selected and at the end
     * of the "play turn" method the current player will be set to play again.
     */
    game.nextPlayer();
    game.invertDirection();
  }
}
