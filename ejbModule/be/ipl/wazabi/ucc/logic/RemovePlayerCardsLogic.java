package be.ipl.wazabi.ucc.logic;

import java.util.Iterator;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;

class RemovePlayerCardsLogic extends BaseCardLogic {

  public RemovePlayerCardsLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 6;
  }

  /**
   * Remove every card except 1 from a user's deck. The targeted user must choose which cards to
   * remove. Additional parameters: Other user's id.
   */
  @Override
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    if (parameters.length != 1) {
      throw new UccException("[RemovePlayerCards] Invalid parameter count",
          IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    GamePlayerEntity otherPlayer = LogicUtil.getGamePlayer(parameters[0], player.getGame());
    if (otherPlayer == null) {
      throw new UccException("Player " + parameters[0] + " is not part of the game",
          IGameUcc.ERRNO_INVALID_PARAMETERS);
    }

    Iterator<CardEntity> cardList = otherPlayer.removeAllCards().iterator();
    int cardToRegive = 1;

    while (cardList.hasNext() && cardToRegive > 0) {
      otherPlayer.addCardToDeck(cardList.next());

      cardToRegive--;
    }

    while (cardList.hasNext()) {
      player.getGame().addCardToDeck(cardList.next());
    }
  }
}
