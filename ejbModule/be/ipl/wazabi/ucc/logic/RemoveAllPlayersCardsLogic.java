package be.ipl.wazabi.ucc.logic;

import java.util.Iterator;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.ucc.exception.UccException;

class RemoveAllPlayersCardsLogic extends BaseCardLogic {

  public RemoveAllPlayersCardsLogic(ICardLogic nextlogic) {
    super(nextlogic);
  }

  @Override
  public int getHandledEffect() {
    return 8;
  }

  /**
   * Leave 2 cards in every player's deck except the user. The targeted users must choose which
   * cards to remove.
   */
  @Override
  public void handleCard(GamePlayerEntity player, String... parameters) throws UccException {
    GameEntity game = player.getGame();

    for (GamePlayerEntity otherPlayer : game.getPlayerList()) {
      if (otherPlayer.equals(player)) {
        continue;
      }

      Iterator<CardEntity> cardList = otherPlayer.removeAllCards().iterator();
      int cardToRegive = 2;

      while (cardList.hasNext() && cardToRegive > 0) {
        otherPlayer.addCardToDeck(cardList.next());

        cardToRegive--;
      }

      while (cardList.hasNext()) {
        game.addCardToDeck(cardList.next());
      }
    }
  }
}
