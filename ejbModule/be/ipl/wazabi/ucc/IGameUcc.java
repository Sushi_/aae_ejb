package be.ipl.wazabi.ucc;

import java.util.Collection;

import javax.ejb.Remote;

import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.ucc.exception.UccException;

@Remote
/**
 * UCC Used for active games logic.
 */
public interface IGameUcc {
  
  int ERRNO_NOT_IN_GAME = 1;
  int ERRNO_INVALID_PLAYER_STATE = 2;
  int ERRNO_NOT_ENOUGH_DICES = 3;
  int ERRNO_INVALID_CARD = 4;
  int ERRNO_CANNOT_USE_CARD = 5;
  int ERRNO_INVALID_PARAMETERS = 6;
  int ERRNO_QUIT_INVALID_GAMESTATE = 7;

  /**
   * Rolls the dices of a player if it is his turn.
   * For each dice with a {@link DiceEntity#DRAW_CARD_FACE} value, add a card in the player's deck.
   * 
   * @throws UccException errno {@link #ERRNO_NOT_IN_GAME} - The player is not in a game or does not exist.
   * @throws UccException errno {@link #ERRNO_INVALID_PLAYER_STATE} - It is not the turn of this player to play.
   * @return The value of the various dices.
   */
  Collection<DiceEntity> rollDices(int userId) throws UccException;

  /**
   * Transfers a dice from the giving player to each one of the players of the list.
   * 
   * Must be done during the player's turn, after the dices have been rolled and before a card is played.
   * The player cannot give more dices than the number of dices with a {@link DiceEntity#GIVE_DICE_FACE} value he received when {@link rollDices(int)} was called.
   *
   * @param userId The user giving the dices.
   * @param destinationUsers The users receiving the dices.
   * @throws UccException errno {@link #ERRNO_NOT_IN_GAME} - The player is not in a game or does not exist.
   * @throws UccException errno {@link #ERRNO_INVALID_PLAYER_STATE} - It is not the turn of this player to play or he hasn't .
   * @throws UccException errno {@link #ERRNO_NOT_ENOUGH_DICES} - Trying to give out more dices than allowed.
   */
  void giveDices(int userId, int destinationUsers[]) throws UccException;
  
  /**
   * Returns the dices an user has in hand.
   * 
   * @param userId The id of the user from which to fetch the dices.
   * @throws UccException errno {@link #ERRNO_NOT_IN_GAME} - The player is not in a game or does not exist.
   */
  Collection<DiceEntity> getDiceCount(int userId) throws UccException;
  
  /**
   * Returns the deck of cards of an user in a game.
   * 
   * @param userId The id of the user from which to fetch the deck of cards.
   * @throws UccException errno {@link ERRNO_NOT_IN_GAME} - The player is not in a game or does not exist.
   */
  Collection<CardEntity> getCards(int userId) throws UccException;
  
  /**
   * Make a player use a card and removes it from his deck.
   *
   * @param userId The id of the user using the card.
   * @param cardId The id of the card to use.
   * @param cardUseParams additional parameters for the card use depending on the card.
   *
   * @throws UccException errno {@link #ERRNO_NOT_IN_GAME} - The player is not in a game or does not exist.
   * @throws UccException errno {@link #ERRNO_INVALID_PLAYER_STATE} - It is not the turn of this player to play or he hasn't rolled his dices yet.
   * @throws UccException errno {@link #ERRNO_NOT_ENOUGH_DICES} - The player doesn't have enough {@link DiceEntity#WAZABI_FACE} dices to play this card.
   * @throws UccException errno {@link #ERRNO_INVALID_CARD} - The card is not owned by this player.
   * @throws UccException errno {@link #ERRNO_INVALID_PARAMETERS} - Invalid card parameter.
   */
  void playCard(int userId, int cardId, String... cardUseParams) throws UccException;
  
  /**
   * Removes a player from his current game.
   * @param playerId The id of the player who whishes to join the game.
   * 
   * @throws UccException errno {@link #ERRNO_NOT_IN_GAME} - The player is not in a game.
   */
  void quitGame(int playerId) throws UccException;
}
