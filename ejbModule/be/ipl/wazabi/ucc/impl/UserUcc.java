package be.ipl.wazabi.ucc.impl;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import be.ipl.wazabi.dal.dao.IPlayerDao;
import be.ipl.wazabi.dal.exception.ConstraintViolationException;
import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.ucc.IUserUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.util.StringUtil;
import io.github.ephys.hasher.IHashHolderDto;
import io.github.ephys.hasher.IStringHasher;

@Singleton
public class UserUcc implements IUserUcc {

  @EJB
  private IPlayerDao playerDao;
  
  @EJB
  private IStringHasher hashHandler;

  @Override
  public PlayerEntity logIn(String username, String passwordAttempt) throws UccException {
    PlayerEntity player = username != null ? playerDao.getByUsername(username) : null;

    if (player == null) {
      throw new UccException("Invalid username " + username, ERRNO_LOGIN_INVALID_USERNAME);
    }

    IHashHolderDto actualPassword = hashHandler.deserialize(player.getPassword());
    if (!hashHandler.matchHash(passwordAttempt.toCharArray(), actualPassword)) {
      throw new UccException("Invalid password " + passwordAttempt, ERRNO_LOGIN_INVALID_PASSWORD);
    }
    
    if (hashHandler.isHashOutdated(actualPassword)) {
      player.setPassword(hashPassword(passwordAttempt));
    }

    return player;
  }

  @Override
  public PlayerEntity register(String username, String password) throws UccException {
    if (StringUtil.isEmpty(username)) {
      throw new UccException("Invalid username, cannot be empty", ERRNO_REGISTER_INVALID_USERNAME);
    }

    if (!StringUtil.isValidPassword(password)) {
      throw new UccException("Invalid password",
          ERRNO_REGISTER_INVALID_PASSWORD);
    }

    try {
      String passwordHash = hashPassword(password);
      return playerDao.insertSafe(new PlayerEntity(username, passwordHash));
    } catch (ConstraintViolationException e) {
      throw new UccException("Invalid username " + username + ", already in use",
          ERRNO_REGISTER_DUPLICATE_USERNAME, e);
    }
  }
  
  private String hashPassword(String password) {
    return hashHandler.serialize(hashHandler.hash(password.toCharArray()));
  }

  @Override
  public PlayerEntity getById(int id) {
    return playerDao.getById(id);
  }
}
