package be.ipl.wazabi.ucc.impl;

import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import be.ipl.wazabi.dal.dao.ICardDao;
import be.ipl.wazabi.dal.dao.IDiceDao;
import be.ipl.wazabi.dal.dao.IGameDao;
import be.ipl.wazabi.dal.dao.IGamePlayerDao;
import be.ipl.wazabi.dal.dao.IPlayerDao;
import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GameEntity.State;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.main.Setup;
import be.ipl.wazabi.ucc.IGameListUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.util.StringUtil;

@Singleton
public class GameListUcc implements IGameListUcc {

  @EJB
  private IGameDao gameDao;

  @EJB
  private IPlayerDao playerDao;

  @EJB
  private IDiceDao diceDao;

  @EJB
  private IGamePlayerDao gamePlayerDao;

  @EJB
  private ICardDao cardDao;

  @EJB
  private Setup configuration;

  @Override
  public List<GameEntity> listGamesByState(State state) {
    return gameDao.getGameByState(state);
  }

  @Override
  public GameEntity joinGame(int gameId, int playerId) throws UccException {

    GameEntity gameEntity = gameDao.getById(gameId);
    if (gameEntity == null || gameEntity.getState() != GameEntity.State.INITIATING) {
      throw new UccException("The game is not waiting for any more players",
          ERRNO_JOIN_INVALID_GAMESTATE);
    }

    PlayerEntity playerEntity = playerDao.getById(playerId);
    if (playerEntity == null || getCurrentGame(playerId) != null) {
      throw new UccException("The player is already in a game or he does not exist.",
          ERRNO_JOIN_INVALID_GAMESTATE);
    }

    GamePlayerEntity gamePlayer = gameEntity.addPlayer(playerEntity);
    gamePlayerDao.insert(gamePlayer);

    // game now meets the start conditions.
    if (gameEntity.getPlayerCount() == configuration.getMinPlayers()) {
      initGame(gameEntity);
    }

    return gameDao.update(gameEntity);
  }

  private void initGame(GameEntity gameEntity) {
    if (gameEntity.getState() != State.INITIATING) {
      throw new IllegalStateException("game already started");
    }

    gameEntity.nextState();

    List<GamePlayerEntity> players = gameEntity.getPlayerList();
    List<DiceEntity> dices = diceDao.getAll();
    List<CardEntity> cards = cardDao.getAll();
    Collections.shuffle(cards);

    int diceIndex = 0, cardIndex = 0;
    for (GamePlayerEntity player : players) {
      // add cards to every players
      for (int i = 0; i < configuration.getStartCardCount(); i++) {
        player.addCardToDeck(cards.get(cardIndex++));
      }

      // add dices to every players
      for (int i = 0; i < configuration.getStartCardCount(); i++) {
        player.addDice(dices.get(diceIndex++));
      }

      // gamePlayerDao.update(player);
    }

    // add the remaining cards to the game deck.
    for (; cardIndex < cards.size(); cardIndex++) {
      gameEntity.addCardToDeck(cards.get(cardIndex));
    }
  }

  @Override
  public GameEntity createGame(String gameName) throws UccException {

    if (StringUtil.isEmpty(gameName)) {
      throw new UccException("'" + gameName + "' is not a valid game name" + gameName,
          ERRNO_CREATE_INVALID_NAME);
    }

    if (!gameDao.getNonFinishedGames().isEmpty()) {
      throw new UccException("Cannot create a game right now, a game is already in progress.",
          ERRNO_JOIN_INVALID_GAMESTATE);
    }

    GameEntity gameEntity = new GameEntity(gameName);

    return gameDao.insert(gameEntity);
  }

  @Override
  public GameEntity getCurrentGame(int playerId) {
    return gameDao.getCurrentGameForPlayer(playerId);
  }

  @Override
  public GameEntity getGameById(int gameId) {
    return gameDao.getById(gameId);
  }

  @Override
  public List<GameEntity> listActiveGames() {
    return gameDao.getNonFinishedGames();
  }
}
