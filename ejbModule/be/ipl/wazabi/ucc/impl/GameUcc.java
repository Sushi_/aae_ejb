package be.ipl.wazabi.ucc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Singleton;

import be.ipl.wazabi.dal.dao.IDiceDao;
import be.ipl.wazabi.dal.dao.IGameDao;
import be.ipl.wazabi.dal.dao.IGamePlayerDao;
import be.ipl.wazabi.dal.dao.IPlayerDao;
import be.ipl.wazabi.entity.CardEntity;
import be.ipl.wazabi.entity.DiceEntity;
import be.ipl.wazabi.entity.GameEntity;
import be.ipl.wazabi.entity.GamePlayerEntity;
import be.ipl.wazabi.entity.PlayerEntity;
import be.ipl.wazabi.entity.GamePlayerEntity.State;
import be.ipl.wazabi.main.Setup;
import be.ipl.wazabi.ucc.IGameUcc;
import be.ipl.wazabi.ucc.exception.UccException;
import be.ipl.wazabi.ucc.logic.CardLogicHandler;

@Singleton
public class GameUcc implements IGameUcc {

  @EJB
  private IGameDao gameDao;

  @EJB
  private IPlayerDao playerDao;

  @EJB
  private IDiceDao diceDao;

  @EJB
  private IGamePlayerDao gamePlayerDao;

  @EJB
  private CardLogicHandler cardLogicHandler;
  
  @EJB
  private Setup configuration;

  @Override
  public Collection<DiceEntity> rollDices(int userId) throws UccException {
    GameEntity game = getCurrentGameForPlayer(userId);
    GamePlayerEntity player = game.getCurrentPlayer();

    if (player.getState() != State.THROWING_DICES) {
      throw new UccException("User is not supposed to throw dices right now, they are in state "
          + player.getState().name(), ERRNO_INVALID_PLAYER_STATE);
    }
    
    for (DiceEntity dice : player.getPlayableDices()) {
      dice.roll(configuration);
      diceDao.update(dice);

      if (dice.getValue() == DiceEntity.DRAW_CARD_FACE) {
        player.addCardToDeck(game.drawCard());
      }
    }

    if (player.countDicesOfType(DiceEntity.GIVE_DICE_FACE) > 0) {
      player.setState(State.GIVING_DICES);
    } else {
      player.setState(State.USING_CARD);
    }
    
    player = gamePlayerDao.update(player);
    gameDao.update(game);

    return player.getPlayableDices();
  }

  @Override
  public void giveDices(int giverId, int destinationUserIds[]) throws UccException {
    GameEntity gameEntity = getCurrentGameForPlayer(giverId);
    GamePlayerEntity currentPlayer = gameEntity.getCurrentPlayer();

    if (currentPlayer.getState() != State.GIVING_DICES) {
      throw new UccException("User is not supposed to give away dices right now, they are in state "
          + currentPlayer.getState().name(), ERRNO_INVALID_PLAYER_STATE);
    }

    Collection<DiceEntity> handDices = currentPlayer.getPlayableDices();

    List<DiceEntity> listDropDice = new ArrayList<>();
    for (DiceEntity dice : handDices) {
      if (dice.getValue() == DiceEntity.GIVE_DICE_FACE) {
        listDropDice.add(dice);
      }
    }

    if (listDropDice.size() < destinationUserIds.length) {
      throw new UccException("Trying to give out more dices than allowed", ERRNO_NOT_ENOUGH_DICES);
    }

    List<GamePlayerEntity> playerList = new ArrayList<>(destinationUserIds.length);
    for (int destinationUserId : destinationUserIds) {
      if (destinationUserId == giverId) { // can't send to self
        continue;
      }
      
      PlayerEntity otherPlayer = playerDao.getById(destinationUserId);
      GamePlayerEntity otherGamePlayer =
          otherPlayer == null ? null : gameEntity.getPlayerInstance(otherPlayer);

      if (otherGamePlayer == null) {
        throw new UccException("The player is not in a game or does not exist.", ERRNO_NOT_IN_GAME);
      }

      playerList.add(otherGamePlayer);
    }

    for (int i = 0; i < playerList.size(); i++) {
      GamePlayerEntity gpe = playerList.get(i);
      gpe.addDice(currentPlayer.removeDice(listDropDice.get(i)));
      gamePlayerDao.update(gpe);
    }

    if (gameEntity.getState() == GameEntity.State.ENDED) {
      gameDao.update(gameEntity);
    } else if (listDropDice.size() == playerList.size()) {
      currentPlayer.setState(State.USING_CARD);
    }
    
    gamePlayerDao.update(currentPlayer);
  }

  @Override
  public Collection<DiceEntity> getDiceCount(int userId) throws UccException {

    GameEntity game = gameDao.getPlayingGameForPlayer(userId);
    if (game == null) {
      throw new UccException("The player is not in a game or does not exist.", ERRNO_NOT_IN_GAME);
    }

    PlayerEntity player = playerDao.getById(userId);
    return game.getPlayerInstance(player).getPlayableDices();
  }

  @Override
  public Collection<CardEntity> getCards(int userId) throws UccException {

    GameEntity gameEntity = gameDao.getPlayingGameForPlayer(userId);
    if (gameEntity == null) {
      throw new UccException("The player is not in a game or does not exist.", ERRNO_NOT_IN_GAME);
    }

    PlayerEntity player = playerDao.getById(userId);
    return gameEntity.getPlayerInstance(player).getPlayableCards();
  }

  @Override
  public void playCard(int userId, int cardId, String... cardUseParams) throws UccException {

    if (cardUseParams == null) {
      throw new UccException("Card parameters cannot be null.",
          ERRNO_INVALID_PARAMETERS);
    }

    GameEntity game = getCurrentGameForPlayer(userId);
    GamePlayerEntity currentPlayer = game.getCurrentPlayer();

    if (currentPlayer.getState() != State.USING_CARD) {
      throw new UccException("User is not supposed to use a card right now, they are in state "
          + currentPlayer.getState().name(), ERRNO_INVALID_PLAYER_STATE);
    }

    if (cardId > 0) {
      CardEntity card = currentPlayer.removeCardFromDeck(cardId);
      if (card == null) {
        throw new UccException("The card is not owned by this player.", ERRNO_INVALID_CARD);
      }
      
      if (card.getEffect().getUseCost() > currentPlayer.countDicesOfType(DiceEntity.WAZABI_FACE)) {
        throw new UccException("The player doesn't have enough curse dices to use this spell.", IGameUcc.ERRNO_NOT_ENOUGH_DICES);
      }

      cardLogicHandler.useCard(card, currentPlayer, cardUseParams);
      game.addCardToDeck(card);
    }

    currentPlayer.setState(State.WAITING);
    if (game.getState() != GameEntity.State.ENDED) {
      game.nextPlayer();
    }

    gameDao.update(game);
  }

  @Override
  public void quitGame(int playerId) throws UccException {

    GameEntity game = gameDao.getCurrentGameForPlayer(playerId);
    if (game == null) {
      throw new UccException("The player is not in a game or does not exist.", ERRNO_NOT_IN_GAME);
    }

    PlayerEntity player = playerDao.getById(playerId);
    GamePlayerEntity gamePlayer = game.getPlayerInstance(player);

    for (CardEntity card : gamePlayer.removeAllCards()) {
      game.addCardToDeck(card);
    }

    gamePlayer.removeAllDices();

    game.removePlayer(player);

    gameDao.update(game);
    gamePlayerDao.delete(gamePlayer);
  }

  private GameEntity getCurrentGameForPlayer(int uid) throws UccException {
    GameEntity game = gameDao.getPlayingGameForPlayer(uid);
    if (game == null) {
      throw new UccException("The player is not in a game or does not exist.", ERRNO_NOT_IN_GAME);
    }

    GamePlayerEntity gameCurrentPlayer = game.getCurrentPlayer();
    if (gameCurrentPlayer.getPlayer().getId() != uid) {
      throw new UccException("It is not the turn of this player to play",
          ERRNO_INVALID_PLAYER_STATE);
    }

    return game;
  }
}
