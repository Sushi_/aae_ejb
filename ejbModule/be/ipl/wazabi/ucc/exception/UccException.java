package be.ipl.wazabi.ucc.exception;

public class UccException extends Exception {

  private static final long serialVersionUID = 6016414914774523287L;

  private final int errno;

  public UccException(Throwable cause, int errno) {
    this(cause.getMessage(), errno, cause);
  }
  
  public UccException(String message, int errno) {
    this(message, errno, null);
  }
  
  public UccException(String message, int errno, Throwable cause) {
    super(message, cause);
    
    this.errno = errno;
  }

  public int getErrno() {
    return errno;
  }
}
